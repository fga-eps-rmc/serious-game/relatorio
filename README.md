## Sobre

Repositório de documentação do projeto BGS.

Link da documentação:
https://relatorio-fga-eps-rmc-serious-game-274af16ace4c463ac68656c946ac.gitlab.io/

## Integrantes

### 2024-1

| Nome | Matricula | Email |
|------|-----------|-------|
| Antonio Igor Carvalho | 180030264| antonioigorcarvalho@gmail.com|
| Douglas Monteles da Silva |190012200 |silvadouglas328@gmail.com|
| Erick Levy Barbosa dos Santos | 180016067| erickfga23@gmail.com|
| Luís Fernando Furtado de Araújo | 180042661 | luiscesm1@gmail.com |
|Rodrigo Carvalho dos Santos | 180027352| ro12062000@gmail.com |
