# Visão do Produto

Nessa dinâmica do Lean inception, dividimos a nossa equipe em três sub-grupos, dois com duas pessoas e uma pessoa sozinha, onde o intuito era cada sub-grupo criar uma visão do produto, posteriormente discutimos sobre cada uma com o intuito de criar uma visão de produto final.

## Visao do Produto 1

![visao1](../../assets/images/leanInception/visaoDoProduto/visaoDoProduto1.png)

## Visao do Produto 2

![visao2](../../assets/images/leanInception/visaoDoProduto/visaoDoProduto2.png)

## Visao do Produto 3

![visao3](../../assets/images/leanInception/visaoDoProduto/visaoDoProduto3.png)

# Resultado

Depois disso decidido com o grupo qual seria a melhor visão de produto, tendo base as três que foram feitas na dinâmica.

![visaoFinal](../../assets/images/leanInception/visaoDoProduto/resultadoFinalVisaoDoProduto.png)