# Revisão Técnica, de Negócio e de UX

A etapa de brainstorm gera diversas funcionalidades, por isso precisamos analisar e filtrar cada uma das funcionalidades em termos de esforço (E), valor de negócio ($) e UX (<3) que seria o quão bom seria essa funcionalidade para o usuário.

Para avaliarmos esses fatores utilizamos uma escala de um a três:

![tabela esforco, negocio, ux](../../assets/images/leanInception/revisaoTecnica/revisao-tecnica.png)

Nessa atividade, pegamos cada funcionalidade retirada do brainstorm e pontuamos cada um seguindo esses três critérios e suas cores:

![Resultado](../../assets/images/leanInception/revisaoTecnica/revisaoTecnicaResultado.png)
