# Jornada de Usuário

A jornada de usuário é feita se baseando na atividade de Persona, para cada persona que foi criada temos uma jornada referente a esta persona, uma jornada de usuário pode ser definida como uma sequência de passos da persona em seu dia-a-dia, onde em algum momento dessa jornada, nosso produto será utilizado por aquela persona.

## Jornada de Usuário 1

![Jornada de usuario 1](../../assets/images/leanInception/jornadaUsuario/jornada1.png)

## Jornada de Usuário 2

![Jornada de usuario 2](../../assets/images/leanInception/jornadaUsuario/jornada2.png)

## Jornada de Usuário 3

![Jornada de usuario 3](../../assets/images/leanInception/jornadaUsuario/jornada3.png)