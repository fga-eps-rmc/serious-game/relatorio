# Sequenciador

Nessa atividade pegamos as funcionalidades feitas na revisão técnica e as ordenamos em termos de prioridade do projeto seguindo as regras do sequenciador:

![Regras](../../assets/images/leanInception/sequenciador/regras.png)

Depois, vendo as questões de tempo, escopo e recursos, marcamos até onde será o provável Mínimo Produto Viável (MVP) e o que será incrementado caso a equipe finalize o MVP antes do prazo da disciplina:

## Resultado:

![Resultado](../../assets/images/leanInception/sequenciador/resultado.png)