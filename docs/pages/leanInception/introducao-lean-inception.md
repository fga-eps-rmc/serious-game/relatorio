# Introdução

O Lean Inception é um framework colaborativo, para decidir em grupo o que será o Minimum Viable Product (MVP), que será construido pela equipe de desenvolvimento. Utilizando técnicas que são a combinação do do Design Thinking e do Lean StartUp, são aplicadas em uma sequência de atividades que geram como um produto final o MVP desejado.

As técnicas utilizadas pela equipe foram:

- Visão do Produto
- O Produto É - NÃO É - FAZ - NÃO FAZ
- Objetivos do Produto/Produto
- Personas
- Jornada de usuário
- Brainstorming de Funcionalidades
- Revisão técnica, de Negócio e de UX
- Sequenciador
- Canvas MVP

# O FIGMA

Todo processo do lean inception foi desenvolvido usando a ferramenta [FIGMA](https://www.figma.com/file/NjxUX6DJUqmigQHgfUuCLZ/Novo-Lean-Inception?type=whiteboard&node-id=0%3A1&t=wcq9uGw1qjRbtBmk-1) e como resultado final tivemos o lean inception abaixo:
![LEAN-INCEPTION-FINAL](../../assets/images/leanInception/leanInception.png)