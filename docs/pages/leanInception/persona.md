Uma persona é uma representação ficticia de usuários, ela é baseada em pessoas reais, comportamentos, objetivos, desafios e preocupações que um usuário do produto teria no seu dia-a-dia. O grupo criou um total de três personas.

## Persona 1

![Persona 1](../../assets/images/leanInception/personas/persona1.png)

## Persona 2

![Persona 2](../../assets/images/leanInception/personas/persona2.png)

## Persona 3

![Persona 3](../../assets/images/leanInception/personas/persona3.png)