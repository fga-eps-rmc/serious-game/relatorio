# Canvas MVP

O canvas MVP é um quadro visual para auxiliar a equipe a alinhar e auxiliar a estratégia do MVP, para que o minimo produto viável possa ser utilizado, para fazer isso dividimos o grupo em dois sub-grupos e fizemos, cada um, o MVP com as informações que tivemos durante o desenvolvimento do Lean Inception.

## MVP 1

![mvp 1](../../assets/images/leanInception/canvasmvp/mvp1.png)

## MVP 2

![mvp 2](../../assets/images/leanInception/canvasmvp/mvp2.png)