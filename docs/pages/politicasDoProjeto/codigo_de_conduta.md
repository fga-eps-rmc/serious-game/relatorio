
## Propósito

O projeto é gerido por estudantes durante a execução da disciplina de Engenharia de Produto de Software da Universidade de Brasília. Assim, com o intuito de promover um ambiente aberto e acolhedor, os estudantes, como contribuintes e mantenedores, comprometem-se a participar ativamente do projeto e da comunidade, proporcionando uma experiência livre de assédio para todos.

## Comportamento

### Comportamentos aceitáveis

- Respeito pelas diferentes opiniões, pontos de vista e experiências.
- Oferecer e receber críticas de forma sempre construtiva.
- Assumir responsabilidade pelos próprios erros e aprender com a experiência.
- Utilizar uma linguagem acolhedora e inclusiva.
- Demonstração de empatia com os membros da comunidade.

### Comportamentos inaceitáveis

- Evitar o uso de linguagem ou imagens com conotação sexual.
- Não praticar trollagem, insultos, depreciação ou ataques pessoais ou políticos.
- Não tolerar o assédio, seja ele público ou não.
- Não divulgar informações pessoais de terceiros sem permissão, como fotos, e-mails pessoais, etc.
- Abster-se de qualquer outra conduta que, dentro do princípio da razoabilidade, possa ser considerada inadequada em um ambiente profissional.

## Responsabilidades

Os mantenedores do projeto têm a responsabilidade de esclarecer os padrões de comportamento estabelecidos no código de conduta. Em caso de dúvida, devem tomar medidas corretivas adequadas e justas em resposta a quaisquer instâncias de comportamentos inaceitáveis. Eles também têm o direito e a responsabilidade de remover, editar ou recusar comentários, confirmações, código e outras contribuições que não estejam alinhados com este Código de Conduta. Além disso, têm o poder de banir temporariamente ou permanentemente qualquer contribuidor por comportamentos considerados inadequados, ameaçadores, ofensivos ou prejudiciais.

## Escopo

Este Código de Conduta é aplicado dentro do projeto e em locais públicos onde um indivíduo esteja representando o projeto ou sua comunidade. Isso inclui, por exemplo, e-mails oficiais sobre o projeto e participação em eventos como representante.



## Referência

ORGANIZATION FOR ETHICAL SOURCE. Contributor Covenant, 2014. Latest Version. Disponível em: https://www.contributor-covenant.org/version/2/1/code_of_conduct/. Acesso em: 13 de abril. de 2024.

## Versionamento

| Data       | Versão | Descrição                      | Autor(es) |Revisor |
| :--------: | :----: | :----------------------------: | :-------: |:-----: |
| 14/04/2024|  0.1   |     codigo de conduta      | [erick levy](https://gitlab.com/IlusionFox) | |
