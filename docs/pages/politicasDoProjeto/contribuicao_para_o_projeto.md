# Contribuindo para o projeto


Antes de enviar uma alteração para este repositório, assegure-se de se responsabilizar por ela 
através das issues do GitHub. Verifique se está seguindo nossa política de branches e commits, e certifique-se de que sua branch segue a checklist de pull request:

* Os commits seguem o padrão de estilo deste projeto.
* A branch está em conformidade com nossa política.
* A integração contínua foi bem-sucedida.
* Os critérios de aceitação foram atendidos.
* Foram realizados testes (quando aplicável) que abrangem as funcionalidades entregues.


Este repositório possui um código de conduta que deverá ser seguido em todas as
interações com ele.

## Processo de criação de issues

1. Verifique se não existe uma issue que já aborde o que você pretende adicionar.
2. Ao criar a issue, aguarde a resposta de um dos responsáveis pelo repositório para sua aprovação.

## Processo de Pull Request

1. Execute os testes em sua máquina que estavam em vigor antes de sua alteração e envie o Pull Request apenas se eles ainda estiverem passando.
2. Aguarde o processo de integração contínua para uma segunda verificação.
3. Ao criar o Pull Request, siga o modelo disponível em docs/pull_request_template.md.
4. Selecione um desenvolvedor do projeto para revisar o seu Pull Request. Além disso, ele será revisado pelo scrum master. Somente após essa revisão conjunta, ele poderá ser aceito.