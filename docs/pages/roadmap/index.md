# Roadmap do Produto

Esse documento contêm todo o planejamento das atividades que serão realizadas no decorrer do semestre. Todas as semanas foram registras e junto a elas foram adicionadas as atividades que serão desenvolvidas.

# Proposta de Roadmap

<figure>
  <img src="../../assets/images/roadmap/RoadmapSeriousGameEPS.jpg" alt="Roadmap do produto">
  <figcaption>Figura 1: Roadmap</figcaption>
</figure>


<br/><br/>

# Versionamento

|Data      |Descrição                                             | Autor |
|----------|------------------------------------------------------| ----- |
|09/04/2024|Criação do documento de roadmap | <a href="https://github.com/DouglasMonteles" target="_blank">@DouglasMonteles</a> |


