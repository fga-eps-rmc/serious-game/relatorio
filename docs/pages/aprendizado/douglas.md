# Objetivo

Esta questão visa entender a percepção individual sobre suas próprias contribuições e desafios enfrentados, além de identificar o nível de autocrítica e capacidade de autoavaliação.

# Detalhamento: Tecnologias e Ferramentas Utilizadas

- NodeJS
- Javascript
- Docker
- docker-compose
- LocalStack
- AWS Lambda
- Serverless Framework
- PostgreSQL
- VsCode
- Playfab
- CBS - Cross-platform Backend Solution (PlayFab)


# Soluções Implementadas

## Atividade: Criar forma de rodar local (ex: docker-compose) do C# PlayFab

**Descrição**: Nesta atividade, o objetivo era encontrar uma forma de executar localmente o código do backend do PlayFab. Esse código realiza a liberação de todos os endpoints relacionados às funcionalidades do playfab, tais como passe de batalha, criação de itens para compra, moeda do jogo, cadastro de usuários, etc. Para finalizar essa atividade, foi necessário utilizar o Docker e docker-compose, para encapsular em containers todos os serviços necessários. Dentre os serviços, está o Azurite, que emula os serviços de Armazenamento do Azure para facilitar o desenvolvimento e os testes locais. O outro serviço consiste na execução do próprio Backend Game as a Service, que é escrito em C# e é executado em um ambiente dotnet. Uma grande dificuldade que surgiu nessa atividade foi que o código fonte do backend depende de uma biblioteca chamada CBS, que é paga e no momento da compra estava custando $89 (oitenta e nove dólares). Ela fornece implementações de classes que são importadas e utilizadas dentro do BGaaS. A conclusão dessa tarefa era importante, pois com ela finalizada, é possível integrar os endpoints dos microsserviços ao playfab e testar o cadastro de informações no banco de dados de longa persistência. Atualmente essa atividade encontra-se pronta para entrar em produção, e encontra-se nesse card do trello.


## Atividade: Criação de Microsserviço em Node com Typescript

**Descrição**: Nessa atividade, o objeto era criar um projeto base escrito em NodeJS com Typescript. Esse código é utilizado por todos os microsserviços. Nesse projeto, eu criei a estrutura dos arquivos, configuração das variáveis de ambiente, configuração do docker e docker-compose, e configuração do banco de dados (postgreSQL). Atualmente, essa atividade também está pronta para produção, e pode ser visualizada no card do trello.


## Atividade: (SampleNode) Bug de acesso às entities dentro do /dist

**Descrição**: Nessa atividade, eu reportei um bug que encontrei na aplicação base dos microsserviços. Trata-se de um erro ao tentar acessar o caminho das migrations, pois esse caminho muda quando era gerado o build da aplicação, em específico dentro do diretório /dist. Como eu reportei esse bug, também forneci a solução para ele se encontra nesse card, e essa correção foi aplicada em todos os microsserviços.


## Atividade: (Stores) Criar micro serviço que recebe os dados de Lojas vindo do PlayFab

**Descrição**: Trata-se da implementação do microsserviço que pega os dados das lojas (PlayStore, AppleStore, etc) quando um usuário do jogo realizar alguma compra. Esses dados passam pelo playfab, e nesse momento, devem ser enviados também para o microsserviço, que vai realizar o cadastro desses dados no banco de dados PostgreSQL. Os dados envolvem o id do usuário, o id do item que foi comprado, a data da compra e a moeda utilizada no jogo para compra. Atualmente essa atividade foi finalizada e aguarda por revisão, e pode ser visualizada nesse card.

## Atividade: Criar código Serverless AWS

**Descrição**: Nessa atividade, o objetivo é conseguir executar os microsserviços, que estavam utilizando o Express para iniciar o servidor, mas que agora precisam ser executados no Serverless AWS. O AWS Lambda é um serviço de computação sem servidor e orientado a eventos que permite executar código para praticamente qualquer tipo de aplicação ou serviço de backend sem provisionar ou gerenciar servidores. Porém, nessa atividade, a ideia é realizar o deploy dos microsserviços localmente, utilizando o LocalStack. Essa ferramenta permite que se desenvolva e teste as aplicações AWS localmente para reduzir o tempo de desenvolvimento e sem custos com provisionamento de recursos no ambiente da AWS. Essa atividade foi finalizada e está em revisão, e pode ser visualizada nesse card.


## Atividade: (Missions) Missões PlayFab (battlepass, daily...)

**Descrição**: Nessa atividade, eu particularmente estou trabalhando na implementação dos endpoints no microsserviço de missões para que ele receba os dados relacionados aos passe de batalha e missões diárias realizadas pelos jogadores e os armazene no banco de dados PostgreSQL. Atualmente, essa atividade ainda está em progresso e pode ser visualizada nesse card.


# Quais foram as principais dificuldades e conflitos que você encontrou durante o desenvolvimento do produto de software?

- Criação do projeto base dos microsserviços, que foi escrito em NodeJs com Typescript. Eu fiquei responsável por essa tarefa, mas meu conhecimentos nessas tecnologias era básico, tendo feito somente alguns projetos com essa stack. Para contornar isso, e entregar a tarefa, eu pesquisei tutoriais na internet que ensinavam a criar um projeto node com typescript do zero, e a como executá-lo no docker.
- Utilização da biblioteca CBS, que foi necessário realizar a compra dela no markplace da unity, para que eu conseguisse entregar a tarefa de executar o código do backend do playfab localmente.
- Criação de lambda functions baseadas nos microsserviços, com a qual eu não tinha nenhuma experiência, e precisei ler bastante da documentação do serverless framework e do aws lambda, para entender como isso funciona.

# Dificuldades Técnicas

- Aquisição da biblioteca [CBS](https://assetstore.unity.com/packages/tools/game-toolkits/cbs-cross-platform-backend-solution-playfab-200638), que custou $89 no momento da compra;
- Configuração do servidor em dotnet, que expõe os endpoints do playfab, pois ele é escrito que C#, linguagem de programação com a qual eu não tinha nenhuma familiaridade.
- Configurar os microserviços para rodarem no AWS Lambda de forma local, com o LocalStack. Pois nunca havia feito nada parecido com isso, e precisei ler bastante da documentação e de fóruns na internet para conseguir realizar essa configuração.

# Conflitos de Equipe

- Durante o Planning Poker, alguns integrantes da equipe discordaram sobre a quantidade de pontos atribuída a uma tarefa. Porém, isso é comum durante o processo de estimativa, e quando havia discrepância entre as pontuações atribuídas a uma tarefa, cada integrante que pontuou diferente dos demais apresentava sua justificativa e tentava convencer os demais ou era convencido pelo demais a seguir com a pontuação que mais se repetiu.

# Contribuições Técnicas

- Criação do projeto base para os microsserviços em NodeJS com Typescript;
- Criação de forma de executar o servidor em dotnet do playfab dentro de containers docker;
- Correção de bugs ao gerar a build do projeto de microsserviço;

# Impacto no Projeto

- Para quem for trabalhar com os endpoints do playfab, não vai precisar fazer nenhuma configuração adicional na sua máquina. Basta ter o docker instalado e executar o comando docker compose up.
- Como os microsserviços serão executados em uma arquitetura serverless, ao finalizar a atividade de configuração do LocalStack, agora é possível subir um ambiente que emula os serviços da AWS, e com isso, é possível fazer o deploy local e realizar testes nas lamba functions sem consumir os serviços reais da AWS e gerar custos desnecessários.

