# Detalhamento: Tecnologias e Ferramentas Utilizadas

- NodeJS
- Javascript
- Docker
- Docker-compose
- PostgreSQL
- VsCode
- Playfab
- Firebase

# Soluções Implementadas

**Documentação de Interface de Desenvolvedor:**
Pesquisei por outros programas/plataformas, qual possuem a opção de desenvolvedor de aplicação utilizando a API do programa/plataforma, dentro desses pode se destacar Playfab, Meta Facebook, Apple e Discord. Fiz uma documentação que apresenta como foi a experiência do fluxo para se conseguir um *dummy example* deste processo de desenvolvedor.

**Microsserviço de Desenvolvedor Ainda Incompleto:** Com base na pesquisa criei o básico para o microsserviço de desenvolvedor, porém não tive sucesso de aplicar todas as funcionalidades a tempo da disciplina.

**ADMIN UI (Desenvolvedor):** Parte web da plataforma do BGS. Em especial a área do desenvolvedor, a qual será utilizado utilizar o microsserviço citado acima. 

# Quais foram as principais dificuldades e conflitos que você encontrou durante o desenvolvimento do produto de software?

- Falta de entendimento no início do projeto sobre o que é o microsserviço de Desenvolvedor, onde a principio eu pensei que seria um login de administrador, que iria ter controle dos cadastros de usuários e do banco de dados. Porém a proposta era totalmente diferente
- Problemas com o Firebase, o qual a execução no Windows enfrentei diversos problemas para faze-lo funcionar. Mas por fim consegui arrumar com ajuda do Cliente.
- Problemas pessoais, durante as ultimas semanas da disciplina minha mãe foi hospitalizada, e dia sim dia não, eu revezava com meu padrasto para ficar com ela no hospital, que fazia eu perder tempo e reuniões.

# Detalhamento:* - *Dificuldades Técnica
- Ao implementar o firebase no Windows, alguns comandos não roda pelo terminal do VsCode, tendo que ser executado diretamente pelo cmd com permissão de administrador.
- Ter que fazer um retrabalho quando foi alinhado a proposta do microsserviço de desenvolvedor.

# Conflitos de Equipe
- O Microsserviço de Desenvolvedor, não foi previamente colocado no MVP do produto, o que me deixou perdido no meu desenvolvimento.

# Contribuições Técnicas
- Documentação: Fiz uma documentação que auxilia a compreensão do fluxo de desenvolvedor.
- Tester: Realização de teste e detecção de erros nos jogos propostos pelo cliente, tanto o iWar quanto AutismyVR.

# Impacto no Projeto:
- Dentro do grupo, eu fui o mais desfocado, demorei para entender minha própria tarefa, não consegui comparacer em todas as reuniões, e atrasei tendo que fazer retrabalho e por fim não consegui entregar minha parte final. Porém pretendo continuar ajudando o desenvolvimento do projeto mesmo após o fim da matéria.
