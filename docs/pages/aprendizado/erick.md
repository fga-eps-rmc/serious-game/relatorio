# Objetivo


Este questionamento busca compreender a percepção pessoal sobre suas próprias contribuições e os desafios enfrentados, além de avaliar o nível de autocrítica e a capacidade de autoavaliação.

# Detalhamento: Tecnologias e Ferramentas Utilizadas

- C#
- NodeJS
- Javascript
- Docker
- docker-compose
- LocalStack
- AWS Lambda
- Serverless Framework
- PostgreSQL
- VsCode
- FIREBASE
- Playfab
- CBS - Cross-platform Backend Solution (PlayFab)
- EUREKA NETFLIX


# Soluções Implementadas

- ADMIN UI: Parte web da plataforma que oferece uma interface para os usuários que ingressam no serviço de "Back End Game as a Service" (BGS). A modularização do site está em andamento, mas ainda não foi finalizada. Esta atividade utiliza React para o front-end do site.

- Chats Global: Atividade de integração do chat do Firebase ao BGS. O desenvolvimento está em andamento e consegui testar as funcionalidades usando aplicações web, mas ainda está em progresso. Atualmente, estou utilizando apenas o Firebase, com a integração dessa funcionalidade planejada para os jogos em desenvolvimento.


# Quais foram as principais dificuldades e conflitos que você encontrou durante o desenvolvimento do produto de software?

- Problema com PlayFab: Inicialmente, enfrentamos dificuldades ao integrar o PlayFab ao Docker. Foram necessárias semanas de trabalho até descobrirmos que a solução era adquirir uma biblioteca específica do PlayFab.

- Problema com Firebase: Tivemos dificuldades ao gerar Cloud Functions no Firebase devido a uma mudança na política de dados do Google, que passou a exigir pagamento por esse serviço. Esse problema foi identificado após vários desafios com o Google Cloud para fazer o Firebase funcionar corretamente.



# Detalhamento:* - *Dificuldades Técnica
- Ao implementar os recursos no Windows, enfrentamos problemas para fazer o projeto funcionar adequadamente. Como este é um projeto que será levado adiante, é crucial que ele funcione em todos os sistemas operacionais.

- Aquisição do Firebase: Foi necessário adquirir o módulo pago do Firebase para poder implementar o deploy.

# Conflitos de Equipe
- Discrepância durante o Planning Poker: Houve discrepâncias durante o Planning Poker, resultando em atividades que poderiam ter sido mais bem estimadas em termos de facilidade, enquanto outras foram subestimadas em dificuldade. No entanto, essa experiência foi útil para perceber que a visão inicial diferiu da final, destacando a importância de revisitar e ajustar estimativas ao longo do projeto.

# Contribuições Técnicas


- Documentação e Atuação como Scrum Master: Responsável por manter a documentação atualizada e desempenhar o papel de Scrum Master, facilitando a comunicação e o andamento do projeto.

- Tester nas Partes Necessárias do Game: Realizou testes nas partes essenciais do jogo para garantir a qualidade e funcionalidade.

- Identificação dos Recursos no Firebase: Auxiliou na identificação e gerenciamento dos recursos necessários no Firebase para a implementação do chat.

- ADMIN UI: Desenvolveu a interface administrativa para facilitar o front-end do projeto.


# Impacto no Projeto:

- Acredito que contribuí significativamente para a equipe, ajudando sempre que alguém precisava e realizando QA quando necessário. Em relação à qualidade do código, considero que está adequada para o que foi implementado por mim. No entanto, os prazos não foram cumpridos conforme o esperado, pois gastei muito mais tempo na tarefa do AppODeal do que o planejado inicialmente.

- Atuação como Scrum Master: Facilitei a comunicação e o andamento do projeto, mantendo a documentação atualizada e ajudando a equipe a seguir as práticas ágeis.

- Facilitação da Ferramenta Firebase: Ajudei a implementar e testar o Firebase, especialmente na gestão do recurso pago, para garantir que ele atendesse aos limites e necessidades de usuários pequenos durante o desenvolvimento do projeto.

- ADMIN UI: Desenvolvi a interface web administrativa do projeto, destinada aos futuros clientes do serviço.

