# Detalhamento: Tecnologias e Ferramentas Utilizadas
- NodeJs
- TypeScript
- TypeOrm
- Docker
- PostgreSQL
- AppODeal
- Netflix Eureka
- C#
- Firebase
- Git
- Gitlab


# Soluções Implementadas
No meu caso iniciei a tarefa de criação de uma funcionalidade que recebe dados que são
disparados quando uma pessoa assiste um anúncio em algum jogo, esse disparo é
realizado pelo AppODeal que está configurado no Unity, o objetivo da tarefa era criar o
backend que recebe esses dados, realizar a descriptografia, validação dos dados e então
caso sejam válidos guardar esses dados no banco de dados relacional e chamar
futuramente o endpoint de recompensa (que seria feito em outra tarefa). Até o momento a
funcionalidade está desenvolvida, porém, não conseguimos concluir os testes de seu
funcionamento por completo, pois o AppODeal não está retornando alguns dados dentro
dessa mensagem criptografada.

Antes de iniciar a atividade descrita acima tivemos que realizar diversos estudos, assistir vídeos e realizar atividades de Quality Assurance (QA) em relação a serviços que já estavam implementados ou que foram implementados ao longo do tempo.

# Quais foram as principais dificuldades e conflitos que você encontrou durante o desenvolvimento do produto de software?

No começo do projeto o primeiro desafio foi tentar entender do que se tratava o projeto, de
início pensei que se tratava de um backend para apenas jogos de tiro, já que foi
apresentado para a gente um vídeo explicando um jogo first person shooter para celular já
funcionando, então fizemos um canvas MVP com base nisso, depois que o projeto foi
explicado para a gente novamente é que descobrimos que se tratava de um backend
genérico, que poderia ser utilizado para diversos tipos de jogos, e então fizemos um novo
canvas MVP, porém o PO já tinha todas as funcionalidades que ele desejava planejadas,
até mesmo com uma arquitetura complexa desenhada com diversas tecnologias, então o
nosso canvas MVP não agregou nenhum valor creio eu. Em relação a entender o projeto
por inteiro no meu caso eu acredito que fiquei com um bom entendimento somente após o
Ilton mostrar para a gente o protótipo do site que de apresentação do produto final que ele
está montando e também explicar como funcionará o modelo de negócio, o público alvo,
como será feito o marketing, etc.

# Objetivo Identificar os principais obstáculos enfrentados pela equipe e as habilidades de resolução de problemas e conflitos, além de avaliar a capacidade de comunicação e colaboração entre os membros da equipe.

# Detalhamento:* - *Dificuldades Técnica

Como descrito anteriormente, uma dificuldade foi a de entender do que se tratava o projeto
realmente, porém a maior dificuldade foi a grande quantidade de tecnologias utilizadas na
arquitetura da solução, demandando muito tempo apenas para entender do que se tratavam
e como funcionavam. A estratégia para mitigar esse problema vinha principalmente do PO,
que fornecia todo o suporte com muita boa vontade.

# Conflitos de Equipe

Em relação a conflitos com a equipe não me recordo de nenhum.

# Contribuições Técnicas

Desenvolvimento da tarefa do endpoint de Recompensa (AppODeal) e realização de QAs e documentação.

# Impacto no Projeto:

Creio que contribuí bem para a equipe, ajudando se alguém
pedia algo ou realizando QAs que eram necessários, em relação à qualidade de código
creio que também está ok aquilo que foi implementado por mim, agora em relação a prazos
não foi o esperado, gastei muito mais tempo na tarefa do AppODeal do que o que foi
planejado inicialmente.
