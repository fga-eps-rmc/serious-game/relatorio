# Objetivo

Esta questão visa entender a percepção individual sobre suas próprias contribuições e desafios enfrentados, além de identificar o nível de autocrítica e capacidade de autoavaliação.

# Detalhamento: Tecnologias e Ferramentas Utilizadas

- Javascript
- TypeScript
- NodeJS
- Express
- Eslint
- Swagger
- Docker
- AWS Lambda
- PostgreSQL
- VsCode
- Playfab
- Firebase
- Git
- Netflix OSS Framework

# Soluções Implementadas

## Atividade: Pipeline DevOps CI/CD Micro Serviço TS (Sample Node)

**Descrição**: O objetivo nessa atividade era de criar a pipeline completa (CI/CD) do template base dos microserviços em typescript que criamos. Essa tarefa contava com o desenvolvimento da pipeline utilizando o gitlab-ci, além de também criar alguns steps importantes de configuração, como: o build da aplicação, uma fase de testes onde o setup da aplicação tem que estar configurado, e por fim, o step de deploy que é integrado com a infraestrutura para fazer o deploy automatizado e integrado com as soluções AWS escolhidas. Implementei os steps e job-runs no gitlab CI, e conseguimos validar o funcionamento dos steps de build e de testes. O step de deploy, optamos por não fazer agora, visto que a infraestrutura ainda não está 100% pronta.  


## Atividade: Criação de usuário e integração/associação usuário Firebase<->PlayFab (Users microservice)

**Descrição**: O objetivo era criar um endpoint no microserviço que recebe dados vindo da cloud function de criação de usuário vindo do playfab. Após isso, o usuário é criado em long term data no microserviço, e é feito um handshake no playfab para criar o usuário lá, e por fim, associar o usuário do microserivço que está associado ao firebase, também ao playfab. A tarefa foi implementada com sucesso.


## Atividade: Atualização de usuário (Users microservice)

**Descrição**: O objetivo é de criar um endpoint para atualização dos usuários no microserviço de users. Há algumas tratativas de dados importantes que não podem ser alteradas como firebaseId e playFabId por exemplo, que foram controladas na model. A tarefa foi implementada com sucesso.


## Atividade: Integração para criação de usuário quando for criado no firebase (Users microservice)

**Descrição**: O objetivo dessa tarefa é de usar a function do firebase para quando houver uma criação de usuário, a function envia uma requisição para o serviço do backend que cria o usuário, faz a associação desse usuário que é do firebase e por fim, faz a integração do playfab. A tarefa foi implementada e está indo para processo de revisão. 


# Quais foram as principais dificuldades e conflitos que você encontrou durante o desenvolvimento do produto de software?

- A arquitetura foi feita em conjunto com o cliente. Por conta disso, muitas tecnologias não eram de conhecimento do grupo, sendo difícil a associação pelo tempo que tínhamos para a matéria.
- Utilização do framework netflix OSS. Foi meu primeiro contato com esse framework e tive muitas dificuldades para entender algumas interfaces e como elas se comunicavam, e principalmente, o motivo de estarmos utilizando ela.
- Entendimento de como as coisas se comunicariam, e principalmente, o que deveria ser consumido apenas em banco de dados não relacionais e o que seria importante levar para um relacional para um long term data.

# Dificuldades Técnicas

- Configuração local do firebase
- Configuração para a comunicação entre os serviços com o Netflix OSS
- Configuração do PlayFab

# Conflitos de Equipe

Acredito que em alguns momentos, o planning poker não foi tão eficiente para deixar claro realmente a divisão de tarefas entre os membros do grupo. Porém, o time se apoiou bastante durante o desenvolvimento e durante os problemas enfrentados.

# Contribuições Técnicas

- Configuração do CI/CD para o microserviço base em typescript que foi utilizado para todos os microserviços
- Serviço de criação de usuário e integração firebase <-> PlayFab
- Serviço de edição de usuário com validações de segurança
- Integração do Firebase function na criação de usuário com microserviço de usuários

# Impacto no Projeto

- Etapas de CI/CD agora rodam no projeto, não sendo necessário rodar localmente para um controle manual, o próprio ambiente do git faz a função de rodar as funções para garantir que os testes estão passando, que o build está funcionando.
- O handshake de usuários do firebase, pro microserviço e por fim pro playfab já está finalizado, fazendo com que o usuário que está no firebase que utiliza a aplicação já tem acesso aos recursos também do playfab, e por fim, já estamos armazenando a longo prazo e de maneira organizada todos esses dados.

