# Review Sprint 4

## 1. Introdução

A Sprint 4 foi realizada entre os dias 04/06/2024 e 18/06/2024, com a participação de todos os membros da equipe. Durante esse período, foram realizadas atividades de planejamento e execução, visando atingir os objetivos estabelecidos para o projeto.

## 2. Objetivos da Sprint

Os principais objetivos da Sprint 4 foram:

- Documentação da sprint.
- Baixar e utilizar o Unity.
- Épicos em andamento :
    - [GBAAS - 35](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/9)
    - [GBAAS - 34](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/10)
    - [GBAAS - 31](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/7)
    - [GBAASS - 32](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/13)
    - [GBAASS - 37](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/11)



## 3. Realizações

Durante a Sprint 4
, a equipe conseguiu realizar as seguintes atividades:

🌌 - Atividade quase finalizada
✔ - Atividade finalizada
❌ - Atividade não finalizada

| Issue |            Título            |        Responsáveis         |        Status      | 
|:-------:|:----------------------------:|:-----------------------------:|:-----------------------------:|
| [#7](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/7) | Sprint 4 -  #31 - Criar micro serviço que recebe os dados de Lojas vindo do PlayFab | [Douglas Monteles](https://github.com/DouglasMonteles) | 🌌 |
| [#8](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/8) | Sprint 4 - #57 - Documentação primeira entrega R1| [Antonio Igor](https://github.com/AntonioIgorCarvalho) e [Erick Levy](https://github.com/Ericklevy) | 🌌 |
| [#9](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/9) | #35 - chat global | [Erick Levy](https://github.com/Ericklevy)| 🌌 |
| [#10](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/10) | Sprint 4 - #34 - Perfil/Profile + PlayFabUID | [Luís Fernando](https://github.com/luis-furtado) e [Rodrigo Carvalho](https://github.com/Rocsantos) | 🌌 |
| [#11](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/11) | Sprint 4 - #37 - EndPoint de Recompensa (integração appodeal) | [Antonio Igor](https://github.com/AntonioIgorCarvalho) | 🌌 |
| [##](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/12) | Sprint 4 - #58 - Review e Planning Sprint - 4° Sprint (04/06/2024 e 18/06/2024)| [Erick Levy](https://github.com/Ericklevy) |  🌌 |
| [#13](https://trello.com/c/gtkaOP2P/73-review-e-planning-sprint-4-sprint-04-06-18-06) | Sprint 4 -#32 - Criar mecanismo de SSO entre o Firebase e o PlayFab | [Luís Fernando](https://github.com/luis-furtado) e [Rodrigo Carvalho](https://github.com/Rocsantos) | 🌌 |

## 4. Dificuldades

Durante a Sprint 4, a equipe enfrentou algumas dificuldades, como:

- Problemas com o firebase em questão de fazer deploy
- Problemas na parte do Playfab com os integrantes Antonio e douglas.
- Problemas técnicos com o Firebase e Appodeal.


## 5. Próximos Passos

Com base nas realizações e dificuldades identificadas na Sprint 3, os próximos passos da equipe incluem:

- Aprofundar o conhecimento nas ferramentas e metodologias utilizadas.
- focar em finalizar os Epicos ate a realease 2

## 6. Papeis

|      Função      |            Nome            |
|------------------|:--------------------------:|
| Desenvolvedores| [Antonio Igor](https://github.com/AntonioIgorCarvalho), [Douglas Monteles](https://github.com/DouglasMonteles), [Rodrigo Carvalho](https://github.com/Rocsantos) |
| DevOps e Desenvolvedor| [Luís Fernando](https://github.com/luis-furtado) |
| Scrum Master e Desenvolvedor| [Erick Levy](https://github.com/Ericklevy)  |

## 7. Versionamento

| Data       | Versão | Descrição                      | Autor(es) | Revisor |
| :--------: | :----: | :----------------------------: | :-------: | :-----: |
| 02/07/2024 |  1.0   | Criação do Documento da Review da 3ª Sprint | [Erick Levy](https://gitlab.com/IlusionFox) | |
