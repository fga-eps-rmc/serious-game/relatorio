# Sprint Planning 3

## 1. Introdução

A Sprint3 foi realizada entre os dias 23/05/2024 e 04/06/2024, com a participação de todos os membros da equipe. Durante esse período, foram realizadas atividades de planejamento e execução, visando atingir os objetivos estabelecidos para o projeto.

## 2. Objetivos da Sprint

Os principais objetivos da Sprint 3 foram:

- Testar o Firebase Local Emulator.
- Documentação da sprint
- Testar o Firebase nas Maquinas 
- Baixar o Unity
- Inicio dos Epicos:
    - [GBAAS - 35](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/9)
    - [GBAAS - 34](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/10)
    - [GBAAS - 31](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/7)
    - [GBAASS - 32](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/13)
    - [GBAASS - 37](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/11)
-  Firebase cloud functions + LocalRunning.




## 3. Realizações

Durante a Sprint 3, a equipe conseguiu realizar as seguintes atividades Inicio dos epicos:

| Issue |            Título            |        Responsáveis         |
|:-------:|:----------------------------:|:-----------------------------:|
| [#7](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/7) | Sprint 3 -  #31 - Criar micro serviço que recebe os dados de Lojas vindo do PlayFab |[Douglas Monteles](https://github.com/DouglasMonteles)|
| [#8](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/8) | Sprint 3 - #57 - Documentação primeira entrega R1| [Antonio Igor](https://github.com/AntonioIgorCarvalho) e [Erick Levy](https://github.com/Ericklevy) | :heavy_check_mark: |
| [#9](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/9) | #35 - chat global | [Erick Levy](https://github.com/Ericklevy)| 
| [#10](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/10) | Sprint 3 - #34 - Perfil/Profile + PlayFabUID | [Luís Fernando](https://github.com/luis-furtado) e [Rodrigo Carvalho](https://github.com/Rocsantos) | 
| [#11](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/11) | Sprint 3 - #37 - EndPoint de Recompensa (integração appodeal) | [Antonio Igor](https://github.com/AntonioIgorCarvalho) | 
| [#12](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/12) | Sprint 3 - #58 - Review e Planning Sprint - 3° Sprint (23/05 - 04/06)| [Erick Levy](https://github.com/Ericklevy) | 
| [#13](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/13) | Sprint 3 -#32 - Criar mecanismo de SSO entre o Firebase e o PlayFab | [Luís Fernando](https://github.com/luis-furtado) e [Rodrigo Carvalho](https://github.com/Rocsantos) | 






## 4. Papeis

|      Função      |            Nome            |
|------------------|:--------------------------:|
| Desenvolvedores| [Antonio Igor](https://github.com/AntonioIgorCarvalho), [Douglas Monteles](https://github.com/DouglasMonteles), [Rodrigo Carvalho](https://github.com/Rocsantos) |
| DevOps e Desenvolvedor| [Luís Fernando](https://github.com/luis-furtado) |
| Scrum Master e Desenvolvedor| [Erick Levy](https://github.com/Ericklevy)  |

## 5. Referências
- The Scrum Guide: https://www.scrumguides.org/scrum-guide.html
- Agile Alliance: https://www.agilealliance.org/
- Scrum Alliance: https://www.scrumalliance.org/
- Agile Manifesto: https://agilemanifesto.org/
- Scrum.org: https://www.scrum.org/
- Mountain Goat Software: https://www.mountaingoatsoftware.com/
- Atlassian: https://www.atlassian.com/agile/scrum/sprint-planning
- Scrum Inc: https://www.scruminc.com/sprint-planning/
- Scrum Alliance: https://www.scrumalliance.org/
- Scrum.org: https://www.scrum.org/
- Mountain Goat Software: https://www.mountaingoatsoftware.com/

## 6. Versionamento

| Data       | Versão | Descrição                      | Autor(es) | Revisor |
| :--------: | :----: | :----------------------------: | :-------: | :-----: |
| 03/05/2024 |  1.0   | Criação do Documento da Planning da 3ª Sprint | [Erick Levy](https://gitlab.com/IlusionFox) | |
