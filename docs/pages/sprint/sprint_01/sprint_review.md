# Review Sprint 1

## 1. Introdução

A Sprint 1 foi realizada entre os dias 25/03/2024 e 07/04/2024, com a participação de todos os membros da equipe. Durante esse período, foram realizadas atividades de planejamento e execução, visando atingir os objetivos estabelecidos para o projeto.

## 2. Objetivos da Sprint

Os principais objetivos da Sprint 1 foram:

- Realizar a Lean Inception do projeto.
- Definir o Roadmap do projeto.
- Testar sample Python.
- Documentar a Sprint.
- Definir políticas do projeto.
- Documentar a arquitetura do projeto.
- Planejar testes e testar sample Python.

## 3. Realizações

Durante a Sprint 1, a equipe conseguiu realizar as seguintes atividades:

- Realização da Lean Inception do projeto.
- Definição do Roadmap do projeto.
- Teste do sample Python.
- Documentação da Sprint.
- Definição das políticas do projeto.
- Documentação da arquitetura do projeto.
- Planejamento dos testes.

🌌 - Atividade quase finalizada
✔ - Atividade finalizada
❌ - Atividade não finalizada

| Issue |            Título            |        Status      | 
|:-------:|:----------------------------:|:-----------------------------:|
| [#1](https://gitlab.com/fga-eps-rmc/serious-game/relatorio/-/issues/1) | Sprint 1 - Lean Inception,Roadmap e testar sample python | ✔ |
| [#1](https://gitlab.com/fga-eps-rmc/serious-game/relatorio/-/issues/1) | Sprint 1 - RoadMap, sample js e Relatórios | ✔ |
| [#1](https://gitlab.com/fga-eps-rmc/serious-game/relatorio/-/issues/1) | Sprint 1 - Documentação da Sprint,politicas e testar o sample python | ✔ |
| [#1](https://gitlab.com/fga-eps-rmc/serious-game/relatorio/-/issues/1) | Definições de papeis  | Todos os membros da equipe |
| [#1](https://gitlab.com/fga-eps-rmc/serious-game/relatorio/-/issues/1) | Sprint 1 - Documento de Arquitetura | ✔|
| [#1](https://gitlab.com/fga-eps-rmc/serious-game/relatorio/-/issues/1) | Sprint 1 - Plano de teste e sample python | ✔ |

## 4. Dificuldades

Durante a Sprint 1, a equipe enfrentou algumas dificuldades, como:

- Ajustes na comunicação entre os membros da equipe.
- Adaptação às ferramentas e metodologias utilizadas.
- Definição de prioridades e distribuição de tarefas.

## 5. Lições Aprendidas

A Sprint 1 proporcionou à equipe diversas lições que serão levadas para

Durante a Sprint 1, a equipe conseguiu realizar as seguintes atividades:

- Realização da Lean Inception do projeto.
- Definição do Roadmap do projeto.
- Teste do sample Python.
- Documentação da Sprint.
- Definição das políticas do projeto.
- Documentação da arquitetura do projeto.
- Planejamento dos testes .




## 4. Dificuldades

Durante a Sprint 1, a equipe enfrentou algumas dificuldades, como:

- Ajustes na comunicação entre os membros da equipe.
- Adaptação às ferramentas e metodologias utilizadas.
- Definição de prioridades e distribuição de tarefas.

## 5. Lições Aprendidas

A Sprint 1 proporcionou à equipe diversas lições que serão levadas para as próximas sprints, tais como:

- Importância da comunicação eficaz.
- Necessidade de adaptação e aprendizado contínuo.
- Valor da definição de prioridades e planejamento.
- Fortalecimento da colaboração e do trabalho em equipe.

## 6. Próximos Passos

Com base nas realizações e dificuldades identificadas na Sprint 1, os próximos passos da equipe incluem:

- Refinar a comunicação interna.
- Aprofundar o conhecimento nas ferramentas e metodologias utilizadas.
- Estabelecer um planejamento

## 7. Papeis

|      Função      |            Nome            |
|------------------|:--------------------------:|
| Desenvolvedores| [Antonio Igor](https://github.com/AntonioIgorCarvalho) e [Douglas Monteles](https://github.com/DouglasMonteles) e [Rodrigo Carvalho](https://github.com/Rocsantos) |
| Devops  e Desenvolvedore| [Luís Fernando](https://github.com/luis-furtado) |
| Scrum Master e Desenvolvedor| [Erick Levy](https://github.com/Ericklevy)  |

## 8. Versionamento

| Data       | Versão | Descrição                      | Autor(es) |Revisor |
| :--------: | :----: | :----------------------------: | :-------: |:-----: |
| 29/04/2024|  1.0  |     Criação do Documento da Planning da 1 sprint     | [erick levy](https://gitlab.com/IlusionFox) | |