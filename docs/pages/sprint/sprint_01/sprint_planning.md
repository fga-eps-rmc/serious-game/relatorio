# Sprint planning


## 1. Introdução
O Sprint Planning é um evento do Scrum que marca o início de uma nova Sprint. Durante essa reunião, a equipe de desenvolvimento se reúne para definir o objetivo da Sprint, selecionar as atividades a serem realizadas e planejar como elas serão executadas.

O Sprint Planning é uma oportunidade para a equipe revisar o Product Backlog, discutir as prioridades do Product Owner e estabelecer um plano de ação para a Sprint. Ele é essencial para garantir que todos os membros da equipe estejam alinhados com os objetivos do projeto e tenham uma compreensão clara do trabalho a ser realizado.

## 2. Objetivo do Sprint Planning
O principal objetivo do Sprint Planning é definir o que será feito durante a Sprint e como isso será feito. Durante a reunião de Sprint Planning, a equipe deve:

- Revisar o Product Backlog e selecionar as atividades que serão realizadas na Sprint.
- Definir o objetivo da Sprint e as metas a serem alcançadas.
- Estimar o esforço necessário para concluir as atividades selecionadas.
- Criar um plano de ação detalhado, incluindo as tarefas a serem realizadas e as dependências entre elas.

O Sprint Planning é uma oportunidade para a equipe se preparar para a Sprint, alinhar expectativas e garantir que todos os membros tenham uma compreensão clara do trabalho a ser realizado.

## 3. Objetivos
| Issue |            Título            |        Responsáveis         | 
|:-------:|:----------------------------:|:-----------------------------:|
| [#1](https://gitlab.com/fga-eps-rmc/serious-game/relatorio/-/issues/1) | Sprint 1 - Lean Inception,Roadmap e testar sample python | [Antonio Igor](https://github.com/AntonioIgorCarvalho) |
| [#1](https://gitlab.com/fga-eps-rmc/serious-game/relatorio/-/issues/1) | Sprint 1 - RoadMap, sample js e Relatórios | [Douglas Monteles](https://github.com/DouglasMonteles)  |
| [#1](https://gitlab.com/fga-eps-rmc/serious-game/relatorio/-/issues/1) | Sprint 1 - Documentação da Sprint,politicas e testar o sample python | [Erick Levy](https://github.com/Ericklevy)  |
| [#1](https://gitlab.com/fga-eps-rmc/serious-game/relatorio/-/issues/1) | Definições de papeis  | Todos os membros da equipe |
| [#1](https://gitlab.com/fga-eps-rmc/serious-game/relatorio/-/issues/1) | Sprint 1 - Documento de Arquitetura | [Luís Fernando](https://github.com/luis-furtado) |
| [#1](https://gitlab.com/fga-eps-rmc/serious-game/relatorio/-/issues/1) | Sprint 1 - Plano de teste e sample python | [Rodrigo Carvalho](https://github.com/Rocsantos) |


## 4. Papeis

|      Função      |            Nome            |
|------------------|:--------------------------:|
| Desenvolvedores| [Antonio Igor](https://github.com/AntonioIgorCarvalho) e [Douglas Monteles](https://github.com/DouglasMonteles) e [Rodrigo Carvalho](https://github.com/Rocsantos) |
| Devops  e Desenvolvedore| [Luís Fernando](https://github.com/luis-furtado) |
| Scrum Master e Desenvolvedor| [Erick Levy](https://github.com/Ericklevy)  |

## 5. Referências
- The Scrum Guide: https://www.scrumguides.org/scrum-guide.html

- Agile Alliance: https://www.agilealliance.org/

- Scrum Alliance: https://www.scrumalliance.org/

- Agile Manifesto: https://agilemanifesto.org/
- Scrum.org: https://www.scrum.org/

- Mountain Goat Software: https://www.mountaingoatsoftware.com/
- Atlassian: https://www.atlassian.com/agile/scrum/sprint-planning
- Scrum Inc: https://www.scruminc.com/sprint-planning/
- Scrum Alliance: https://www.scrumalliance.org/
- Scrum.org: https://www.scrum.org/
- Mountain Goat Software: https://www.mountaingoatsoftware.com/
- Atlassian: https://www.atlassian.com/agile/scrum/sprint-planning
- Scrum Inc: https://www.scruminc.com/sprint-planning/
- Scrum Alliance: https://www.scrumalliance.org/
- Scrum.org: https://www.scrum.org/
- Mountain Goat Software: https://www.mountaingoatsoftware.com/
- Atlassian: https://www.atlassian.com/agile/scrum/sprint-planning
- Scrum Inc: https://www.scruminc.com/sprint-planning/
- Scrum Alliance: https://www.scrumalliance.org/
- Scrum.org: https://www.scrum.org/
- Mountain Goat Software: https://www.mountaingoatsoftware.com/
- Atlassian: https://www.atlassian.com/agile/scrum/sprint-planning
- Scrum Inc: https://www.scruminc.com/sprint-planning/
- Scrum Alliance: https://www.scrumalliance.org/
- Scrum.org: https://www.scrum.org/
- Mountain Goat Software: https://www.mountaingoatsoftware.com/
- Atlassian: https://www.atlassian.com/agile/scrum/sprint-planning
- Scrum Inc: https://www.scruminc.com/sprint-planning/
- Scrum Alliance: https://www.scrumalliance.org/
- Scrum.org: https://www.scrum.org/
- Mountain Goat Software: https://www.mountaingoatsoftware.com/
- Atlassian

## 6. Versionamento

| Data       | Versão | Descrição                      | Autor(es) |Revisor |
| :--------: | :----: | :----------------------------: | :-------: |:-----: |
| 29/04/2024|  1.0  |     Criação do Documento da Planning da 1 sprint     | [erick levy](https://gitlab.com/IlusionFox) | |