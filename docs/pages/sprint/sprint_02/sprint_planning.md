# Sprint Planning - Sprint 2

## 1. Introdução
O Sprint Planning é um evento do Scrum que marca o início de uma nova Sprint. Durante essa reunião, a equipe de desenvolvimento se reúne para definir o objetivo da Sprint, selecionar as atividades a serem realizadas e planejar como elas serão executadas.

O Sprint Planning é uma oportunidade para a equipe revisar o Product Backlog, discutir as prioridades do Product Owner e estabelecer um plano de ação para a Sprint. Ele é essencial para garantir que todos os membros da equipe estejam alinhados com os objetivos do projeto e tenham uma compreensão clara do trabalho a ser realizado.

## 2. Objetivo do Sprint Planning
O principal objetivo do Sprint Planning é definir o que será feito durante a Sprint e como isso será feito. Durante a reunião de Sprint Planning, a equipe deve:

- Revisar o Product Backlog e selecionar as atividades que serão realizadas na Sprint.
- Definir o objetivo da Sprint e as metas a serem alcançadas.
- Estimar o esforço necessário para concluir as atividades selecionadas.
- Criar um plano de ação detalhado, incluindo as tarefas a serem realizadas e as dependências entre elas.

O Sprint Planning é uma oportunidade para a equipe se preparar para a Sprint, alinhar expectativas e garantir que todos os membros tenham uma compreensão clara do trabalho a ser realizado.

## 3. Objetivos
| Issue |            Título            |        Responsáveis         | 
|:-------:|:----------------------------:|:-----------------------------:|
|  [#3](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/3) | Sprint 2 - Estudos sobre Firebase, Emulação e [GBAAS-41] TypeORM working + Docker TypeORM + husky pre commit hook JS | [Antonio Igor](https://github.com/AntonioIgorCarvalho), [Douglas Monteles](https://github.com/DouglasMonteles), [Erick Levy](https://github.com/Ericklevy), [Luís Fernando](https://github.com/luis-furtado), [Rodrigo Carvalho](https://github.com/Rocsantos) |
| [#4](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/4) | Sprint 2 - Implementação de CI/CD | [Douglas Monteles](https://github.com/DouglasMonteles), [Luís Fernando](https://github.com/luis-furtado) |
| [#5](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/5) | Sprint 2 - Avaliação e Ajustes do Back-end com PlayFab | [Antonio Igor](https://github.com/AntonioIgorCarvalho), [Douglas Monteles](https://github.com/DouglasMonteles), [Erick Levy](https://github.com/Ericklevy), [Luís Fernando](https://github.com/luis-furtado), [Rodrigo Carvalho](https://github.com/Rocsantos) |
| [#6](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/6) | Sprint 2 - Avaliação e Ajustes do Back-end com PlayFab | [Douglas Monteles](https://github.com/DouglasMonteles) |

## 4. Papeis

|      Função      |            Nome            |
|------------------|:--------------------------:|
| Desenvolvedores| [Antonio Igor](https://github.com/AntonioIgorCarvalho), [Douglas Monteles](https://github.com/DouglasMonteles), [Rodrigo Carvalho](https://github.com/Rocsantos) |
| DevOps e Desenvolvedor| [Luís Fernando](https://github.com/luis-furtado) |
| Scrum Master e Desenvolvedor| [Erick Levy](https://github.com/Ericklevy)  |

## 5. Referências
- The Scrum Guide: https://www.scrumguides.org/scrum-guide.html
- Agile Alliance: https://www.agilealliance.org/
- Scrum Alliance: https://www.scrumalliance.org/
- Agile Manifesto: https://agilemanifesto.org/
- Scrum.org: https://www.scrum.org/
- Mountain Goat Software: https://www.mountaingoatsoftware.com/
- Atlassian: https://www.atlassian.com/agile/scrum/sprint-planning
- Scrum Inc: https://www.scruminc.com/sprint-planning/
- Scrum Alliance: https://www.scrumalliance.org/
- Scrum.org: https://www.scrum.org/
- Mountain Goat Software: https://www.mountaingoatsoftware.com/

## 6. Versionamento

| Data       | Versão | Descrição                      | Autor(es) | Revisor |
| :--------: | :----: | :----------------------------: | :-------: | :-----: |
| 29/04/2024 |  1.0   | Criação do Documento da Planning da 2ª Sprint | [Erick Levy](https://gitlab.com/IlusionFox) | |


## 7. Atividades da Sprint 2

### Antônio Igor
- Estudo sobre Firebase
- Emulação do Firebase
- Teste do Sample com seus passos

### Douglas Monteles
- Estudo sobre Firebase
- Emulação do Firebase
- Teste do Sample JS com seus passos
- Suporte ao Back-end com PlayFab

### Erick Levy
- Estudo sobre Firebase
- Documentação da Sprint
- Quadro do conhecimento da 2ª Sprint
- Passo a passo do Firebase vídeo 1 e emulação
- Verificação do Sample JS

### Luís Fernando
- Estudo sobre Firebase
- Emulação do Firebase
- Teste do Sample JS com seus passos
- Implementação de CI/CD no repositório sample

### Rodrigo Carvalho
- Teste do Sample JS com seus passos
