# Review Sprint 2

## 1. Introdução

A Sprint 2 foi realizada entre os dias 08/05/2024 e 04/06/2024, com a participação de todos os membros da equipe. Durante esse período, foram realizadas atividades de planejamento e execução, visando atingir os objetivos estabelecidos para o projeto.

## 2. Objetivos da Sprint

Os principais objetivos da Sprint 2 foram:

- Implementar o TypeORM com Docker.
- Configurar o husky pre-commit hook para JavaScript.
- Implementar CI/CD e testar o pipeline.
- Configurar o Firebase Local Emulator.
- Avaliar e ajustar o Back-end com PlayFab.
- Documentação da sprint

## 3. Realizações

Durante a Sprint 2, a equipe conseguiu realizar as seguintes atividades:

🌌 - Atividade quase finalizada
✔ - Atividade finalizada
❌ - Atividade não finalizada

| Issue |            Título            |        Responsáveis         |        Status      | 
|:-------:|:----------------------------:|:-----------------------------:|:-----------------------------:|
| [#3](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/3) | Sprint 2 -  [GBAAS-41] TypeORM working + Docker TypeORM + husky pre-commit hook JS | [Antonio Igor](https://github.com/AntonioIgorCarvalho), [Douglas Monteles](https://github.com/DouglasMonteles), [Erick Levy](https://github.com/Ericklevy), [Luís Fernando](https://github.com/luis-furtado), [Rodrigo Carvalho](https://github.com/Rocsantos) | ✔ |
| [#4](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/4) | Sprint 2 - Implementação de CI/CD e teste pipeline [GBAAS-22]: Pipeline steps refactored | [Luís Fernando](https://github.com/luis-furtado) | ✔ |
| [#5](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/5) | Sprint 2 -  [GBAAS-15] Firebase Local Emulator | [Antonio Igor](https://github.com/AntonioIgorCarvalho), [Douglas Monteles](https://github.com/DouglasMonteles), [Erick Levy](https://github.com/Ericklevy), [Luís Fernando](https://github.com/luis-furtado),  | ✔ |
| [#6](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/6) | Sprint 2 - Avaliação e Ajustes do Back-end com PlayFab | [Douglas Monteles](https://github.com/DouglasMonteles) | ✔ |

## 4. Dificuldades

Durante a Sprint 2, a equipe enfrentou algumas dificuldades, como:

- Ajustes na comunicação entre os membros da equipe.
- Adaptação às ferramentas e metodologias utilizadas.
- Definição de prioridades e distribuição de tarefas.

## 5. Lições Aprendidas

A Sprint 2 proporcionou à equipe diversas lições que serão levadas para as próximas sprints, tais como:

- Importância da comunicação eficaz.
- Necessidade de adaptação e aprendizado contínuo.
- Valor da definição de prioridades e planejamento.
- Fortalecimento da colaboração e do trabalho em equipe.

## 6. Próximos Passos

Com base nas realizações e dificuldades identificadas na Sprint 2, os próximos passos da equipe incluem:

- Refinar a comunicação interna.
- Aprofundar o conhecimento nas ferramentas e metodologias utilizadas.
- Estabelecer um planejamento mais detalhado e eficaz para as próximas sprints.

## 7. Papeis

|      Função      |            Nome            |
|------------------|:--------------------------:|
| Desenvolvedores | [Antonio Igor](https://github.com/AntonioIgorCarvalho), [Douglas Monteles](https://github.com/DouglasMonteles), [Rodrigo Carvalho](https://github.com/Rocsantos) |
| DevOps e Desenvolvedor | [Luís Fernando](https://github.com/luis-furtado) |
| Scrum Master e Desenvolvedor | [Erick Levy](https://github.com/Ericklevy) |

## 8. Versionamento

| Data       | Versão | Descrição                      | Autor(es) | Revisor |
| :--------: | :----: | :----------------------------: | :-------: |:-----: |
| 22/05/2024 |  1.0   | Criação do Documento da Sprint 2 | [Erick Levy](https://gitlab.com/IlusionFox) | |
