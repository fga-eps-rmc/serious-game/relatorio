# Quadro do Conhecimento e Data de Versionamento

## Quadro do Conhecimento - Sprint 5

O quadro do conhecimento, também conhecido como "knowledge board", é uma ferramenta visual utilizada para compartilhar e organizar o conhecimento dentro de uma equipe ou projeto. Ele pode ser usado para registrar informações importantes, como conceitos, ideias, dúvidas, lições aprendidas e melhores práticas.

Um quadro do conhecimento geralmente é dividido em seções ou colunas, representando diferentes categorias ou tópicos relevantes para o projeto. Cada item de conhecimento é registrado em um cartão ou nota adesiva, permitindo que a equipe visualize e atualize facilmente as informações.

O quadro do conhecimento é uma forma eficaz de promover a colaboração, o compartilhamento de conhecimento e a transparência dentro da equipe. Ele pode ser usado durante todo o ciclo de vida do projeto, desde a fase de planejamento até a entrega e manutenção.


Durante a quinta  sprint ou sprint final.

![Quadro do Conhecimento da Sprint-5](/docs/assets/images/quadro-conhecimento-5.PNG)
