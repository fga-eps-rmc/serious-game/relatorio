# Sprint Planning 5
## 1. Introdução

A Sprint 5 foi realizada entre os dias 18/06/2024 e 09/07/2024, com a participação de todos os membros da equipe. Durante esse período, foram realizadas atividades de planejamento e execução, visando atingir os objetivos estabelecidos para o projeto.

## 2. Objetivos da Sprint

Os objetivos principais da Sprint 4.

Foram finalizar os Epicos que foram iniciados
- Documentar a Sprint
- Epicos em andamento:
    - [GBAAS - 35](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/9)
    - [GBAAS - 34](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/10)
    - [GBAAS - 31](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/7)
    - [GBAASS - 32](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/13)
    - [GBAASS - 37](https://gitlab.com/kpihunters/GBaaS/documentation-unb/-/issues/11)

## 3. Realizações

Durante a Sprint 5, a equipe focou em finalizar alguns epicos e seus respectivos problemas 




## 4. Papeis

|      Função      |            Nome            |
|------------------|:--------------------------:|
| Desenvolvedores| [Antonio Igor](https://github.com/AntonioIgorCarvalho), [Douglas Monteles](https://github.com/DouglasMonteles), [Rodrigo Carvalho](https://github.com/Rocsantos) |
| DevOps e Desenvolvedor| [Luís Fernando](https://github.com/luis-furtado) |
| Scrum Master e Desenvolvedor| [Erick Levy](https://github.com/Ericklevy)  |

## 5. Referências
- The Scrum Guide: https://www.scrumguides.org/scrum-guide.html
- Agile Alliance: https://www.agilealliance.org/
- Scrum Alliance: https://www.scrumalliance.org/
- Agile Manifesto: https://agilemanifesto.org/
- Scrum.org: https://www.scrum.org/
- Mountain Goat Software: https://www.mountaingoatsoftware.com/
- Atlassian: https://www.atlassian.com/agile/scrum/sprint-planning
- Scrum Inc: https://www.scruminc.com/sprint-planning/
- Scrum Alliance: https://www.scrumalliance.org/
- Scrum.org: https://www.scrum.org/
- Mountain Goat Software: https://www.mountaingoatsoftware.com/

## 6. Versionamento

| Data       | Versão | Descrição                      | Autor(es) | Revisor |
| :--------: | :----: | :----------------------------: | :-------: | :-----: |
| 09/07/2024 |  1.0   | Criação do Documento da Planning da 5ª Sprint | [Erick Levy](https://gitlab.com/IlusionFox) | |