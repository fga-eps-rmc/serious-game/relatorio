# Review Sprint 5

## 1. Introdução

A Sprint 4 foi realizada entre os dias  18/06/2024 e 09/07/2024, com a participação de todos os membros da equipe. Durante esse período, foram realizadas atividades de planejamento e execução, visando atingir os objetivos estabelecidos para o projeto na parte final.

## 2. Objetivos da Sprint

Os principais objetivos da Sprint 5 foram:

- Finalizar as atividades


## 3. Realizações

Durante a Sprint 4
, a equipe conseguiu realizar as seguintes atividades:

🌌 - Atividade quase finalizada
✔ - Atividade finalizada
❌ - Atividade não finalizada

EPICOS - são atividades dentro de uma atividade maior
Atividade - Uma atividade Basica 

| NUMERO |            Título            |        Responsáveis         |        Status      |
| :----------------------------:|:----------------------------:|:-----------------------------:|:-----------------------------:|
| [# EPICO 37 ](https://trello.com/c/jfIho99y/37-missions-endpoint-de-recompensa-integra%C3%A7%C3%A3o-appodeal) | (Missions) EndPoint de Recompensa (integração appodeal) | [Antonio Igor](https://github.com/AntonioIgorCarvalho) | 🌌 |
| [# EPICO 55 ](https://trello.com/c/lp9CfnKi/55-missions-appodeal-callback-lib-iso-currency-locale-validation) | (missions) APPODEAL - Callback + Lib ISO Currency & Locale validation | [Antonio Igor](https://github.com/AntonioIgorCarvalho) | 🌌 |
| [# EPICO 29 ](https://trello.com/c/tPqXRkSL/29-criar-forma-de-rodar-local-ex-docker-compose-do-c-playfab?search_id=57bed2eb-c4df-430a-8243-fe5fa43ce650) | Criar forma de rodar local (ex: docker-compose) do C# PlayFab | [Douglas Monteles](https://github.com/DouglasMonteles) | ✔ |
| [# EPICO 21 ](https://trello.com/c/0S9r0Z38/21-cria%C3%A7%C3%A3o-de-microsservi%C3%A7o-em-node-com-typescript?search_id=78733b40-2a7d-4b24-b71f-c391c5c45df3) |Criação de Microsserviço em Node com Typescript | [Douglas Monteles](https://github.com/DouglasMonteles) | ✔ |
| [# EPICO 27 ](https://trello.com/c/gsApYD77/27-criar-c%C3%B3digo-serverless-aws) |Criar código Serverless AWS | [Douglas Monteles](https://github.com/DouglasMonteles) | ✔ |
| [# EPICO 62 ](https://trello.com/c/jRVNTXSw/62-admin-developers-panel-ui) | (Admin) *Developers Panel* - UI |[Rodrigo Carvalho](https://github.com/Rocsantos) , [Luís Fernando](https://github.com/luis-furtado) , [Erick Levy](https://github.com/Ericklevy) | 🌌 |
| [# EPICO 35 ](https://trello.com/c/tvlPO4lo/35-firebase-chat-global) |(Firebase) Chat Global | [Erick Levy](https://github.com/Ericklevy) | 🌌 |
| [# EPICO 22 ](https://trello.com/c/st5T8Ye7/22-pipeline-devops-ci-cd-micro-servi%C3%A7o-ts) |Pipeline DevOps CI/CD Micro Serviço TS | [Luís Fernando](https://github.com/luis-furtado) | ✔ |
| [# EPICO 34 ](https://trello.com/c/ApnU1LCD/34-users-perfil-profile-playfabuid) | (Users) Perfil/Profile + PlayFabUID | [Luís Fernando](https://github.com/luis-furtado) |🌌 |
| [# EPICO 32 ](https://trello.com/c/nomLtQ7a/32-developers-criar-mecanismo-de-sso-de-app?filter=dateLastActivity:fourWeeks) | (Developers) Criar mecanismo de SSO de App | [Rodrigo Carvalho](https://github.com/Rocsantos) | 🌌|
| [# Atividade 12 ](https://trello.com/c/UMLCTBcz/12-fazer-o-micro-servi%C3%A7o-funcionar-localhost-standalone-pythonflask) | Fazer o Micro Serviço funcionar localhost standalone (Python<>Flask) | Todos Integrantes | ✔ |
| [# Atividade 15 ](https://trello.com/c/0xqEbRLC/15-firebase-setup-dev-sample-local) | Firebase Setup Dev Sample Local | Todos Integrantes | ✔ |
| [# Atividade 61 ](https://trello.com/c/N7jaX1Nj/61-samplenode-bug-de-acesso-%C3%A0s-entities-dentro-do-dist) |(SampleNode) Bug de acesso às entities dentro do /dist | Todos Integrantes | ✔ |
https://trello.com/c/N7jaX1Nj/61-samplenode-bug-de-acesso-%C3%A0s-entities-dentro-do-dist





## 4. Dificuldades

Finalização dos itens do MVP


## 5. Papeis

|      Função      |            Nome            |
|------------------|:--------------------------:|
| Desenvolvedores| [Antonio Igor](https://github.com/AntonioIgorCarvalho), [Douglas Monteles](https://github.com/DouglasMonteles), [Rodrigo Carvalho](https://github.com/Rocsantos) |
| DevOps e Desenvolvedor| [Luís Fernando](https://github.com/luis-furtado) |
| Scrum Master e Desenvolvedor| [Erick Levy](https://github.com/Ericklevy)  |

## 6. Versionamento

| Data       | Versão | Descrição                      | Autor(es) | Revisor |
| :--------: | :----: | :----------------------------: | :-------: | :-----: |
| 09/07/2024 |  1.0   | Criação do Documento da Review da 5ª Sprint | [Erick Levy](https://gitlab.com/IlusionFox) | |
