# Relatórios

Essa página visa registrar todas as reuniões realizadas pelo grupo, bem como quaisquer resultado alcançado e decisão que foi tomada. Os relatórios estão organizados pela data em que ocorreu o evento, basta clicar em uma data para visualizar as informações que foram registradas.

<br/><br/>

# Versionamento

|Data      |Descrição                                             |
|----------|------------------------------------------------------|
|09/01/2024|Criação do documento que explica a seção de relatórios|
