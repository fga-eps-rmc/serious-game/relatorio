# Dia 09/04/2024

**Data:** 9 de abril de 2024  
**Horário:** 19:30 - 21:30  
**Local:** Sala Online no Discord

## Participantes

- Antonio Igor Carvalho (Desenvolvedor)
- Douglas da Silva Monteles (Desenvolvedor)
- Erick Levy Barbosa dos Santos (Scrum Master e Desenvolvedor)
- Luis Fernando Furtado de Araujo (DevOps e Desenvolvedor)

## Agenda

1. **Continuação da Criação do [Lean Inception](https://www.figma.com/file/NjxUX6DJUqmigQHgfUuCLZ/Novo-Lean-Inception?type=whiteboard&node-id=104%3A1311&t=inTmtFj3opvhYf69-1)**
2. **Documentação no Repositório**
3. **Disponibilização do Protótipo Interativo**
4. **Outros assuntos**

## Decisões e Discussões

### 1. Continuação da Criação do Lean Inception

- Grupo de reuniu no Discord;
- Foi dado prosseguimento a criação do Lean Inception por parte do grupo de forma colaborativa utilizando o Figma;
- Foi realizada a etapa do Brainstorming de funcionalidades;
  - Todos os participantes da reunião contribuíram;
  - Foi utilizado o documento de atividades fornecido pelo Product Owner (PO);
- Foi realizada a etapa de Revisão Técnica, de Negócios e de UX;
  - Todos os participantes da reunião contribuíram;
- Foi realizada a etapa do Sequenciador;
  - Todos os participantes da reunião contribuíram;
  - O sequenciador gerou dúvidas a respeito de como a disposição das atividades nos cards geram o MVP e o Incremento, porém o integrante Antônio Igor sugeriu que todos os cards até o indicativo do MVP definem propriamente o MVP. A partir dali, seriam o Incremento e por fim, os cards com funcionalidades que serão implementadas se houver tempo hábil.
- Foi realizada a etapa do Canvas MVP;

### 2. Documentação no Repositório

- O integrante Douglas com a ajuda do integrante Antônio Igor conseguiu criar a estrutura da documentação do projeto, incluindo endereço público para o acesso da página web com todas as informações sobre o projeto que está sendo desenvolvido;
- Link da documentação: [documentação](https://relatorio-fga-eps-rmc-serious-game-274af16ace4c463ac68656c946ac.gitlab.io/)

### 3. Disponibilização do Protótipo Interativo

- O integrante Douglas criou e disponibilizou o acesso ao protótipo interativo, criado no Figma, sobre as telas do jogo.
- Link: [Protótipo Interativo](https://www.figma.com/proto/c0zkbWuAmFRvKy2UQHj3Pv/iWar?node-id=2-4&starting-point-node-id=2%3A4&mode=design&t=n9IPtfFgBqILhxW3-1)

### 4. Outros Assuntos

- 

## Próximos Passos

- 

## Conclusão

A reunião foi produtiva, proporcionando uma oportunidade para a equipe compartilhar atualizações, discutir desafios e definir planos para avançar. As questões identificadas estão sendo tratadas de forma proativa, e a equipe está comprometida em alcançar os objetivos do projeto dentro do prazo e dos padrões de qualidade estabelecidos.

<br/><br/>

# Versionamento

|Data      |Descrição                                             | Autor |
|----------|------------------------------------------------------| ----- |
|09/04/2024|Criação do documento que reúne os registros das discussões/decisões da reunião que foi realizada | <a href="https://github.com/DouglasMonteles" target="_blank">@DouglasMonteles</a> |
|17/04/2024|Alteração de nome de participante | Antonio Igor Carvalho |