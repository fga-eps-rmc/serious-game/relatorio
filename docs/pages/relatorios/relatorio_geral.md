# Relatório Geral

## 1. Canvas MVP

O termo "Canvas MVP" refere-se a uma abordagem para desenvolver produtos ou serviços, especialmente em startups ou projetos inovadores. "MVP" significa "Minimum Viable Product" (Produto Mínimo Viável), enquanto "Canvas" se refere a uma representação visual, muitas vezes em forma de quadro ou diagrama, que ajuda a organizar e comunicar ideias de forma clara e concisa.

<figure>
  <img src="/assets/images/leanInception/canvasmvp/mvp1.png" alt="Canvas MVP1" />
  <figcaption>Figura 1: Primeira versão do Canvas MVP</figcaption>
</figure>

<figure>
  <img src="/assets/images/leanInception/canvasmvp/mvp2.png" alt="Canvas MVP2" />
  <figcaption>Figura 2: Segunda versão do Canvas MVP</figcaption>
</figure>

## 2. Cronograma

Em desenvolvimento.

## 3. Configuração do Repositório

Para a documentação, o repositório ja foi totalmente configurado. Optamos por utilizar o GitLab como nosso repositório de documentação e de implementação do código do projeto. Atualmente contamos com a organização [🎮 Game Backend as a Service (GBaaS) 🌐](https://gitlab.com/kpihunters/GBaaS).

- Game Backend as a Service (GBaaS): Organização com os repositórios das implementações dos microsserviços e documentações, é nessa organização que o projeto será desenvolvido até o final da disciplina.

A organização está com os integrantes do grupo cadastrados e permite que consigamos cloná-los, implementar funcionalidades e realizar merge requests.

## 4. Definição do Backlog

O backlog do projeto foi definido pelo product owner e é possível visualizar todos os seus componentes no [documento de arquitetura](/pages/documentacao/arquitetura/).

## 5. Configuração Ambiente

Decidimos que a configuração do ambiente será por meio da utilização do Docker e docker compose. Dado que estamos trabalhando no contexto de microsserviços, essa opção pode resultar em uma economia de tempo dos desenvolvedores com configurações de frameworks e bancos de dados. Portanto, todo o ambiente de desenvolvimento será gerenciado pelo containers docker. Os microsserviços serão gerenciados pelo discover Eureka framework, fornecido pelo Netflix OSS.

Para facilitar e padronizar o desenvolvimento de novos micro serviços foi desenvolvido uma aplicação base em Node.js, todos os novos micro serviços serão feitos com base em um fork dessa implementação base.

## 6. Kanban - tarefas

Todas as tarefas foram elaboradas com o auxílio do software Trello, onde organizamos os Épicos e permitimos que cada integrante do grupo selecionasse um ou mais épico para trabalhar durante a sprint (sprints definidas com o tempo de uma semana). Abaixo está a tabela com a lista de cada épico definido e o seu(s) respectivo(s) responsável(is).

| Id  | Título                                            | Responsável |
|-----|---------------------------------------------------| ----------- |
| #22 |Pipeline DevOps CI/CD Micro Serviço TS| <a href="https://gitlab.com/luis-furtado" target="_blank">@luis-furtado</a> |
| #28 |Criar mecanismos de autenticação de APP entre sistemas| <a href="https://github.com/Rocsantos" target="_blank">@Rocsantos</a> |
| #29 |Criar forma de rodar local (ex: docker-compose) do C# PlayFab| <a href="https://github.com/DouglasMonteles" target="_blank">@DouglasMonteles</a> |
| #31 |Criar micro serviço que recebe os dados de Lojas vindo do PlayFab| <a href="https://github.com/DouglasMonteles" target="_blank">@DouglasMonteles</a> e <a href="https://gitlab.com/spiker32" target="_blank">@spiker32</a> e <a href="https://gitlab.com/ErickLevy" target="_blank">@ErickLevy</a> |
| #32 |Criar mecanismo de SSO entre o Firebase e o PlayFab| <a href="https://gitlab.com/luis-furtado" target="_blank">@luis-furtado</a> |
| #33 |Missões|<a href="https://gitlab.com/spiker32" target="_blank">@spiker32</a> |
| #34 |Perfil/Profile + PlayFabUID| <a href="https://github.com/Rocsantos" target="_blank">@Rocsantos</a> |
| #35 |Chat Global| <a href="https://gitlab.com/spiker32" target="_blank">@spiker32</a> e <a href="https://gitlab.com/ErickLevy" target="_blank">@ErickLevy</a> |
| #36 |Ranking| <a href="https://gitlab.com/spiker32" target="_blank">@spiker32</a> |
| #37 |EndPoint de Recompensa (integração appodeal)| <a href="https://gitlab.com/spiker32" target="_blank">@spiker32</a> |
| #38 |Amigos| <a href="https://gitlab.com/ErickLevy" target="_blank">@ErickLevy</a> |

## 7. Quadro do Conhecimento

O "Quadro de Conhecimentos" é uma ferramenta ou estrutura conceitual utilizada para organizar e visualizar o conhecimento em um determinado domínio, área de estudo ou projeto. Essa abordagem é frequentemente adotada em campos como educação, gestão do conhecimento, desenvolvimento de produtos e pesquisa.

Para o nosso caso, vamos atualizar o quadro sempre no final de cada sprint e analisar a evolução individual e em grupo relacionada ao conhecimento obtido.

<figure>
  <img src="/assets/images/quadro-conhecimento.png" alt="Quadro do Conhecimento da Sprint-1" />
  <figcaption>Figura 4: Quadro do Conhecimento da Sprint-1</figcaption>
</figure>

## 8. Protótipo de Alta Fidelidade

Um "protótipo de alta fidelidade" é uma representação visual e interativa de um produto ou sistema que se assemelha muito à versão final em termos de aparência, funcionalidade e interação. Ele é criado usando ferramentas de design e desenvolvimento mais avançadas e geralmente é usado em fases posteriores do processo de design, após a validação de conceitos e protótipos de baixa fidelidade.

No nosso caso, trata-se da interface do jogo FPS em que estamos trabalhando. Abaixo segue a apresentação do protótipo criado com o software Figma e modelado para interfaces de dispositivos móveis.

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="100%" height="720" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Fproto%2Fc0zkbWuAmFRvKy2UQHj3Pv%2FiWar%3Fnode-id%3D2-4%26starting-point-node-id%3D2%253A4%26mode%3Ddesign%26t%3DBmN8Yac7trVQoSCj-1" allowfullscreen></iframe>

## 9. Épicos - Planning Poker

Na metodologia ágil, um Épico é uma unidade de trabalho grande e abstrata que encapsula uma funcionalidade ou conjunto de funcionalidades que não podem ser facilmente completadas em uma única iteração ou sprint. Os Épicos são divididos em tarefas menores e mais gerenciáveis durante o processo de planejamento e execução do projeto.

Já o Planning Poker é uma técnica de estimativa usada em equipes ágeis para determinar o esforço ou a complexidade relativa de uma determinada tarefa ou item de trabalho. Os membros da equipe atribuem pontos de história (ou outras unidades de medida) às tarefas, com base em sua percepção do esforço necessário para completá-las. Isso ajuda a equipe a chegar a um consenso sobre a carga de trabalho e a prioridade das tarefas.

<figure>
  <img src="/assets/images/Kabantarefas.png" alt="Planning poker" />
  <figcaption>Figura 5: Planning poker dos épicos</figcaption>
</figure>

## 10. Pitch

Um pitch é uma apresentação concisa e persuasiva de uma ideia, projeto, produto ou serviço, com o objetivo de capturar a atenção e o interesse do público-alvo. Os elementos-chave incluem:

- **Apresentação do Problema:** Destaque um problema ou necessidade.
- **Solução:** Apresente a solução oferecida.
- **Proposta de Valor:** Destaque os benefícios e vantagens.
- **Modelo de Negócio:** Explique a viabilidade econômica.
- **Apresentação da Equipe:** Destaque as habilidades da equipe.
- **Chamada para Ação:** Conclua com uma ação específica.

Um pitch eficaz é conciso, convincente e deixa uma impressão positiva, gerando interesse para futuras ações. Um vídeo será gravado no futuro.

### 10.1 Introdução:

Nesta apresentação, discutiremos o Backend de Jogo como Serviço (GaaS), uma infraestrutura crucial para proporcionar uma experiência de jogo eficiente.

### 10.2 Contextualização:

O GaaS é uma arquitetura que usa micro serviços para integrar o backend do PlayFab com a nuvem do Firebase e também um backend próprio, com o objetivo de fornecer funcionalidades que podem ser utilizadas por diversos jogos diferentes de forma escalável e personalizada.

### 10.3 O que é GaaS:

É uma arquitetura que fornece serviços compartilhados e escaláveis para vários jogos, incluindo autenticação de usuários, armazenamento de dados, gerenciamento de conteúdo e funcionalidades para jogos.

### 10.4 Micro serviços:

São componentes independentes e especializados que realizam tarefas específicas, oferecendo flexibilidade, escalabilidade e facilidade de manutenção.

### 10.5 Não acoplamento:

Refere-se à capacidade dos componentes do sistema funcionarem independentemente, melhorando a performance ao permitir otimizações individuais sem afetar outros serviços.

### 10.6 Melhorias na performance:

A abordagem de não acoplamento permite escalar serviços individualmente e implementar atualizações de forma incremental, garantindo uma experiência consistente para os jogadores.

### 10.7 Conclusão:

O GaaS é essencial para uma experiência de jogo de alta qualidade. A adoção de micro serviços e o não acoplamento melhoram significativamente a performance do sistema em um ambiente multijogos.

### 10.8 Fechamento:

Agradeço por participarem desta discussão. Espero que tenham compreendido a importância do não acoplamento e seu impacto positivo na performance das soluções em jogos multijogos.

## 11. Scrum
 A escolha do Scrum deve-se ao uso de *Sprints*, que funcionam em pequenos ciclos de 7-10 dias (tempo determinado previamente pela equipe). Esses ciclos ajudam a manter o controle dos prazos de entrega.

* **Sprint Planning:** Realizado no final de cada sprint, que possuem a duração de uma semana, geralmente coincide com a terça-feira, mas pode ser realizado as quintas-feira também, a depender de algum imprevisto.

* **Sprint Review:** Realizado nas terças ou quintas para checar o andamento da equipe com as atividades da semana.

* **Produto Backlog:** Lista de ideias e recursos que devem ser completados ao longo da semana.
  
* **Daily Scrum**: É uma reunião diária no framework ágil Scrum. Seu objetivo principal é fornecer uma oportunidade para que a equipe se sincronize, compartilhe informações e faça planos para o próximo ciclo de trabalho. São realizadas todas as terças e quintas, às 19h e podem ser reuniões online pelo Google Meet ou via mensagens de texto no grupo do telegram.


## 12. Risco - Requisitos

Em desenvolvimento.

## 13. PIPELINE CI/CD - TESTES

A fase atual está em progresso.

## 14. Repositórios

Atualmente contamos com os seguintes repositórios:

Micro serviços:

- ⚡️ missions
- 📈 rankings
- 🧑🏽‍💻 developers
- 🤼‍♂️ users
- 🛍️ Stores

Documentação da matéria:

- 🗃️ Documentation

Outros repositórios:

- 🔥 Firebase
- 🧪 Sample Service NodeJS
- ▶️ PlayFab
- 🎮 Unity GBaaS Template
- 🧪 Sample Service NodeJS - Security policy project
- 📜 GBaaS-Docs
- 🏭 stack
- 🐍 Sample Service Python
- 🌐 GBaaS-Site-LandingPage
- 🛠️ Config
- ⚡Serverless

Estes repositórios foram fornecidos pelo Product Owner (PO), e com base neles, estamos trabalhando nas implementações das tarefas definidas nos épicos. O grupo concordou, por meio de votação, que utilizaríamos o nodejs como base para a criação dos microsserviços. Consonante a isso, no repositório `Sample Service JS` foi submetido um Merge Request com a implementação da base do projeto em nodejs com typescript que será utilizada.
