# 1. Introdução

### 1.1 Objetivo
Este documento descreve a arquitetura de software para o Backend Game as a Service. Seu objetivo é fornecer uma visão geral da estrutura do sistema, suas principais componentes e as interações entre elas.

### 1.2 Escopo

O escopo deste documento abrange a arquitetura de alto nível do sistema BGS, incluindo suas principais funcionalidades, tecnologias utilizadas e decisões arquiteturais.

### 1.3 Visão Geral

Este documento será segmentado da seguinte forma:

1. Introdução: Apresentará a organização do documento, junto a uma breve finalidade do software.
2. Representação de Arquitetura: Demonstra a arquitetura adotada no trabalho.
3. Restrições e Metas de Arquitetura: Exibe os requisitos de usabilidade do software e os propósitos que influenciam a aplicação.

# 2 Representação da arquitetura

### 2.1 Arquitetura Geral

![Visão geral da arquitetura da aplicação](../../assets/images/architecture.svg)

A imagem acima trás uma visão geral do que será desenvolvido e seus componentes, foi construída pelo product owner, nela está representado o fluxo de um jogo e mostrando quais serviços que serão desenvolvidos estão sendo utilizados, sendo eles com a utilização do Firebase, Playfab e backend próprio (representado por uma nuvem).

O ecossistema será apoiado por algumas ferramentas para auxiliar no desenvolvimento que serão descritas abaixo:

#### 2.1.1 Netflix OSS

É um framework para desenvolver e solucionar problemas padrões de arquitetura em microsserviços. O objetivo é principalmente resolucionar problemas, descoberta de serviço, balanceamento de carga e tolerância a falhas.

#### 2.1.2 NodeJS

Para os serviços, iremos utilizar o framework NodeJS com algumas dependências como TypeORM e Express para nos auxiliar no desenvolvimento, sendo mais fácil a implementação e manutenção de requisições ao banco de dados e também disponibilidade do serviço através de API.

#### 2.1.3 PostgreSQL

Cada serviço terá um banco próprio e utilizaremos o banco não relacional PostgreSQL, focando em garantir confiabilidade, escalabilidade, conformidade com padrões e flexibilidade.

#### 2.1.4 AWS

Para configuração e personalização, utilizaremos alguns serviços da AWS como ECS (Elastic Container Service) para gerenciamento dos clusters, S3 para subir e gerenciar imagens de containers e RDS para gerenciar os bancos de dados da aplicação.

#### 2.1.4 Docker

Ferramenta para gerar um ambiente isolado e construído especificamente para a equipe que será utilizado para facilitar o desenvolvimento do projeto.

#### 2.1.4 Git

Ferramenta de versionamento que será usada em conjunto com o GitHub para salvar os dados do decorrer do projeto, possibilitando a hospedagem e a geração de backups do mesmo. 

#### 2.1.4 SonarQube

Ferramenta para geração de análise e métricas do código como segurança, estabilidade e performance.

#### 2.1.4 Gitlab CI

Ferramenta utilizada para integração continua.

# 3. Metas e Restrições da Arquitetura

São metas de Arquitetura:

Disponibilizar aos clientes um serviço fácil de comunicação para aplicações gerais de jogos.
Desacoplamento e independência entre outros serviços.
Monitoramento e escalabilidade dos serviços.
Infraestrutura escalável por módulo/produto.


São restrições de Arquitetura:

Serviços centrais como os do Backoffice e o Gamification não devem se comunicar diretamente com o cliente, apenas através de interfaces e APIs Gateways.
Conexão necessária com a internet.

# 4. Referências

https://sce.uhcl.edu/helm/RationalUnifiedProcess/webtmpl/templates/a_and_d/rup_sad.htm
https://medium.com/@skpallewatta92/microservices-with-netflix-oss-spring-boot-and-non-jvm-applications-2d762768921a
https://guiadohost.com/

# Versionamento

|Data      |Descrição                                             | Autor |
|----------|------------------------------------------------------| ----- |
|15/04/2024|Criação inicial do documento de arquitetura.| <a href="https://gitlab.com/luis-furtado" target="_blank">@luis-furtado</a> |
|02/06/2024 |Atualização da arquitetura | Antonio Igor Carvalho |