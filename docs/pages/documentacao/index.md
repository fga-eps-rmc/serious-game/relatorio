# Documentação

Essa página visa centralizar todas as documentações dos serviços que envolvem o BGS (Backend Game Service).

<br/><br/>

# Versionamento

|Data      |Descrição                                             |
|----------|------------------------------------------------------|
|15/04/2024|Criação do documento que explica a seção de documentação|
