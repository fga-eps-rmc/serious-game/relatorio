# Plano de Testes

Este documento detalha o Plano de Testes para o grupo Backend Game Service, que será aplicado no decorrer do desenvolvimento do projeto ao longo o semestre. Ele também seguirá as indicações do *Product Owner(PO)* Ilton, para ter melhor aproveitamento.

## BDD

O projeto será desenvolvido utilizando a metodologia de teste [**Desenvolvimento Orientado ao Comportamento (BDD)**](https://pt.wikipedia.org/wiki/Behavior_Driven_Development), esta é uma metodologia ágil que encoraja a colaboração entre os desenvolvedores e outros setores. No nosso caso, está diretamente ligado ao *PO*, buscando **verificação** e **validação**.

As práticas do BDD consiste em:

1. Envolver as partes interessadas no processo através de Outside-in Development (Desenvolvimento de Fora para Dentro);
1. Usar exemplos para descrever o comportamento de uma aplicação ou unidades de código;
1. Automatizar os exemplos para prover um feedback rápido e testes de regressão;
1. Usar deve (should em inglês) na hora de descrever o comportamento de software para ajudar esclarecer responsabilidades e permitir que funcionalidades do software sejam questionadas;
1. Usar simuladores de teste (mocks, stubs, fakes, dummies, spies) para auxiliar na colaboração entre módulos e códigos que ainda não foram escritos;

## Teste Unitário

Para este projeto, também utilizaremos Testes Unitários. Testes Unitários se entende como testes que propõem aferir a corretude do código em sua menor fração. Em nosso trabalho, como backend service e microsserviço, se entende como menor fração cada uma das funções que são executadas durante uma requisição ao backend.

Os Testes Unitários nos ajudará com o [BDD](#bdd) nos tópicos 3, 4 e 5. Com isto poderemos entender melhor como a aplicação **deve** funcionar e **não deve** funcionar, e permitirá que as futuras implementações, não haja divergências de ideias sobre o desenvolvimento do trabalho.

## Cobertura de Código

Para este projeto também utilizaremos Cobertura de Código, para compreender se todo o código foi devidamente testado.

## Referências
- BEHAVIOR DRIVEN DEVELOPMENT, aberto. In: WIKIPÉDIA: a enciclopédia livre. Disponível em: [wikipedia.org/wiki/Behavior_Driven_Development](https://wikipedia.org/wiki/Behavior_Driven_Development) [🇧🇷](https://pt.wikipedia.org/wiki/Behavior_Driven_Development)
- Unit Testing, aberto. In: WIKIPÉDIA: a enciclopédia livre. Disponível em: [wikipedia.org/wiki/Unit_testing](https://en.wikipedia.org/wiki/Unit_testing) [🇧🇷](https://pt.wikipedia.org/wiki/Teste_de_unidade)
- Code coverage, aberto. In: WIKIPÉDIA: a enciclopédia livre. Disponível em: [wikipedia.org/wiki/Code_coverage](https://en.wikipedia.org/wiki/Code_coverage) [🇧🇷](https://pt.wikipedia.org/wiki/Cobertura_de_código)

# Versionamento

|Data      |Descrição                                             | Autor |
|----------|------------------------------------------------------| ----- |
|17/04/2024| Criação do Documento de Plano de Teste | <a href="https://github.com/Rocsantos" target="_blank">@Rocsantos</a> |