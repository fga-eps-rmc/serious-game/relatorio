# Quadro do Conhecimento

## 1. Introdução

O Quadro do Conhecimento é uma ferramenta utilizada por equipes de desenvolvimento de software para acompanhar e visualizar o conhecimento acumulado pelos desenvolvedores ao longo das sprints de um projeto. Ele serve como um registro dinâmico do domínio técnico e das habilidades individuais de cada membro da equipe.

## 2. Objetivo da Metodologia nas Sprints

O objetivo principal do Quadro do Conhecimento é promover a transparência e a colaboração dentro da equipe, fornecendo uma visão clara das habilidades disponíveis e das lacunas de conhecimento que precisam ser abordadas. Isso ajuda a garantir que o trabalho seja distribuído de forma equilibrada e que as tarefas sejam atribuídas aos membros mais adequados.

Além disso, o Quadro do Conhecimento facilita o planejamento de capacitação e treinamento, permitindo que a equipe identifique áreas de melhoria e tome medidas para fortalecer suas habilidades.

## 3. Quadros das Sprints

Durante as sprints, os membros da equipe atualizam o Quadro do Conhecimento com base nas novas habilidades adquiridas, tecnologias exploradas e desafios enfrentados. Isso pode incluir:

- Novas linguagens de programação aprendidas
- Frameworks ou bibliotecas utilizadas
- Ferramentas de desenvolvimento dominadas
- Conceitos de arquitetura de software compreendidos

O Quadro do Conhecimento pode ser representado de diversas formas, como uma planilha compartilhada, um quadro Kanban virtual ou até mesmo um documento de texto. O importante é que seja de fácil acesso e atualizado regularmente pela equipe.

### Sprint 1 - Quadro do Conhecimento
![Quadro do Conhecimento da Sprint-1](/assets/images/quadro-conhecimento.png)
![Quadro do Conhecimento da Sprint-2](/assets/images/quadro-conhecimento-2.png)

## 4. Versionamento

Assim como o código-fonte de um projeto é versionado, o Quadro do Conhecimento também pode ser versionado para acompanhar sua evolução ao longo do tempo. Isso permite que a equipe mantenha um histórico das mudanças e observe como o conhecimento individual e coletivo tem progredido.

Ao versionar o Quadro do Conhecimento, é possível identificar tendências, avaliar o impacto das iniciativas de capacitação e ajustar a estratégia de desenvolvimento de competências conforme necessário.

| Data       | Versão | Descrição                      | Autor(es) |Revisor |
| :--------: | :----: | :----------------------------: | :-------: |:-----: |
| 23/04/2024|  1.0  |     padrão de commit      | [erick levy](https://gitlab.com/IlusionFox) | |
