# Metodologias e Ferramentas


## 1. Introdução
ste documento tem como objetivo apresentar as metodologias que serão utilizadas ao longo do projeto, bem como as ferramentas que serão empregadas.

### 1.1 Scrum


O Scrum e o Kanban são duas metodologias ágeis populares amplamente utilizadas no desenvolvimento de software, mas também aplicáveis em diversos outros contextos. Ambas têm o objetivo de melhorar a eficiência, a transparência e a flexibilidade nos processos de trabalho.

O Scrum é como uma corrida de revezamento, onde uma equipe trabalha em ciclos chamados de "sprints", que geralmente duram de uma a quatro semanas. Cada sprint começa com uma reunião de planejamento, onde são definidas as metas e as tarefas a serem realizadas. Durante o sprint, a equipe se reúne diariamente em um breve encontro chamado de "daily stand-up", onde cada membro compartilha o que fez no dia anterior, o que planeja fazer hoje e se enfrentou algum impedimento. No final de cada sprint, há uma revisão, onde o trabalho concluído é demonstrado aos stakeholders, e uma retrospectiva, onde a equipe analisa o que funcionou bem e o que pode ser melhorado para os próximos sprints.

Sendo assim, o Scrum surge para dividir o projeto em pequenas etapas, onde cada etapa possui um ciclo que deve ser seguido: Planejar, Construir, Testar e Revisar.

Existem 3 papéis essenciais para o trabalho funcionar:
* Scrum Master
* Product Owner
* Scrum Team (desenvolvedores, testadores, etc, ...)

E 3 artefatos que fazer parte da documentação:
* Product Backlog
* Sprint Backlog - São ditadas atravês de histórias de usuário.
* Gráfico de Burndown

### 1.2 Kanban

Já o Kanban é mais como uma linha de produção em uma fábrica, onde o trabalho flui de forma contínua e é visualizado em um quadro (kanban) dividido em colunas representando os estágios do processo. As tarefas são representadas por cartões que se movem pelo quadro à medida que progridem de um estágio para o próximo. Não há sprints fixos no Kanban, e o trabalho é priorizado conforme a capacidade da equipe e a demanda do cliente. O foco está em limitar o trabalho em progresso (WIP) para evitar sobrecarga e manter um fluxo constante de entrega.


### 1.3 **XP**
Uma das principais preocupações da metodologia XP é o bem estar da equipe. A prática de pair programming é um dos diferenciais, pois nivela a equipe de desenvolvimento.

A mentalidade do XP se baseia em:
* Escrita de testes
* Refatoração e melhoria do código
* Proximidade com o cliente

## 2 **Escolha da(s) metodologia(s)**

Optamos por integrar ambas as metodologias, buscando aquela que melhor se adapte ao nosso time para desenvolver o "Back Game as a Service" sem impactar negativamente o desempenho da equipe. Pretendemos aproveitar o que há de melhor em cada uma delas para aplicar em nosso projeto.

Construir ou evoluir um software com o mínimos de erros, dentro do prazo e custos pré-estabelecidos não é uma tarefa fácil. Para isso, as metodologias ágeis trouxeram grande ajuda.

Porém, ainda é dificil se adaptar a tantos processos. A combinação de metodologias, sejam elas tradicionais ou ágeis, permite eliminar essa complexidade desnecessária. Para as metodologias ágeis, o processo de combinação é mais fácil, uma vez que muitas possuem algumas características em comum.

### 2.1 **SCRUM**
A escolha do Scrum deve-se ao uso de *Sprints*, que funcionam em pequenos ciclos de 7-10 dias (tempo determinado previamente pela equipe). Esses ciclos ajudam a manter o controle dos prazos de entrega.

* **Sprint Planning:** Realizado nas terças ou quintas para planejar as atividades da semana.

* **Sprint Review:** Realizado nas terças ou quintas para checar o andamento da equipe com as atividades da semana.

* **Produto Backlog:** Lista de ideias e recursos que devem ser completados ao longo da semana.

### 2.2 **Kanban**
Faremos uso de *Checklists*, que é uma abordagem muito simples e efetiva, assim facilitando bastante a aderência pela equipe. Para acompanhar as atividades usaremos um quadro e o dividiremos em:

* **TO DO** - Atividades que ainda precisam ser realizadas.
* **Doing** - Atividades em andamento.
* **Done** - Atividades que foram finalizadas e aprovadas pela equipe.

### 2.3. **XP**
A metodologia XP foi escolhida por ser um método de desenvolvimento que preza o bem estar da equipe e a confiabilidade do produto. Adotamos algumas boas práticas como:

* **Pair Programming(Programação em Pares):** Prática que ajuda a nivelar o conhecimento da equipe e garante códigos mais robustos, por ter dois pontos de vista alinhados.

* **Integração contínua:** Utilizaremos ferramentas de teste unitário para melhor manutenabilidade do software.

* **Padronização do código:** Todo código é desenvolvido seguindo um padrão, qualquer que seja, mas toda equipe deve seguir o mesmo padrão. Dessa forma, todos da equipe terão a mesma visão do código.


## 3 Ferramentas


### 3.1. Discord

O Discord é uma das principais ferramentas de comunicação da equipe. Um servidor dedicado foi criado exclusivamente para interações internas, incluindo reuniões e pareamentos.

Adicionalmente, utilizamos o servidor da disciplina para comunicação com os clientes do projeto e o professor.

### 3.2 Telegram
Além do Discord, a comunicação do time também é feita pelo Telegram, onde são feitas discussões mais objetivas e pontuais.



### 3.3 Figma

O Figma será utilizado para desenvolver protótipos de documentos, entre outras finalidades.

### 3.4 Gitlab


Os códigos e a criação dos documentos para o produto serão armazenados em um repositório no GitLab. Este repositório servirá como um local centralizado para colaboração, controle de versão e gerenciamento de todo o trabalho relacionado ao desenvolvimento do produto.


## 4. Referências bibliográficas

> Deisy Braz S., COMBINAÇÃO DE MÉTODOS ÁGEIS NO PROCESSO DE DESENVOLVIMENTO DE SOFTWARE: UM ESTUDO DE CASO. Disponível em: <https://periodicos.uniarp.edu.br/index.php/ignis/article/download/1133/570/4664>

> SCRUM. Disponível em: <http://www.desenvolvimentoagil.com.br/scrum/>

> T.L., Métodos ágeis: o que são e como impactam o seu negócio? Disponível em: [https://www.lumis.com.br/a-lumis/blog/metodos-ageis...](https://www.lumis.com.br/a-lumis/blog/metodos-ageis.htm#:~:text=As%20metodologias%20%C3%A1geis%20s%C3%A3o%20uma,revistos%20com%20os%20m%C3%A9todos%20%C3%A1geis.)


## 9.Versionamento

|    Data    | Versão |      Descrição       |                   Autor(a)                    |                   Revisor(a)                    |
| ---------- | ------ | -------------------- | --------------------------------------------- | ----------------------------------------------- |
| 14/04/2024|  0.1   |     criação do documento      | [erick levy](https://gitlab.com/Ericklevy) | |
